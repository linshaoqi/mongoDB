package com.jeecg;

import java.util.List;
import java.util.Map;

public class TestMongoDB {
	private static final String _uuid = "8950720c-4bf9-470a-81c0-3d9703855a0a";
	private static final String databaseName = "testMongoDb.xml";
	private static final String tableName = "test";

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// new TestMongoDB().createDatabase(databaseName);
		new TestMongoDB().addData(databaseName, tableName);

		// new TestMongoDB().update(databaseName, tableName);

		// new TestMongoDB().delete(databaseName, tableName);
		// new TestMongoDB().loadAllData(databaseName, tableName);

	}

	public void createDatabase(String databaseName) {
		MongoDB m = new MongoDB();
		m.createDatabase(databaseName);
	}

	public void addData(String databaseName, String tableName) {

		Person p1 = new Person();
		p1.setPno("10000011");
		p1.setName("zhangsan");
		p1.setGender("女");
		p1.setAge(11);

		Person p2 = new Person();
		p2.setPno("10000012");
		p2.setName("lisi");
		p2.setGender("女");
		p2.setAge(12);

		Person p3 = new Person();
		p3.setPno("10000013");
		p3.setName("sunqi");
		p3.setGender("女");
		p3.setAge(13);

		Person p4 = new Person();
		p4.setPno("10000014");
		p4.setName("linwaimao");
		p4.setGender("女");
		p4.setAge(14);

		MongoDB m = new MongoDB();
		try {
			m.addData(databaseName, tableName, p1);
			m.addData(databaseName, tableName, p2);
			m.addData(databaseName, tableName, p3);
			m.addData(databaseName, tableName, p4);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void update(String databaseName, String tableName) {

		Person p = new Person();
		p.set_uuid(_uuid);
		p.setPno("20000001");
		p.setName("李四");
		p.setGender("女");
		p.setAge(50);

		MongoDB m = new MongoDB();
		try {
			m.updateData(databaseName, tableName, p);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void delete(String databaseName, String tableName) {
		Person p = new Person();
		p.set_uuid(_uuid);
		MongoDB m = new MongoDB();
		try {
			m.deleteData(databaseName, tableName, p);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void loadAllData(String databaseName, String tableName) {
		MongoDB m = new MongoDB();
		try {
			List<Map> list = m.loadAlldata(databaseName, tableName);
			for (Map map : list) {
				System.out.println(map);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
