package com.jeecg;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.google.gson.Gson;

public class MongoDB {
	private static final String _uuid = "_uuid";

	public boolean createDatabase(String databaseName) {
		boolean flag = true;

		Document document = new Document();
		Element root = new Element("database");
		document.addContent(root);

		Element table1 = new Element("table");
		table1.setAttribute("name", "test");

		Element table2 = new Element("table");
		table2.setAttribute("name", "system.users");

		Element table3 = new Element("table");
		table3.setAttribute("name", "system.indexes");
		root.addContent(table1);
		root.addContent(table2);
		root.addContent(table3);

		try {
			FileOutputStream fos = new FileOutputStream(databaseName);
			XMLOutputter out = new XMLOutputter();
			out.output(document, fos);
		} catch (IOException e) {
			flag = false;
			e.printStackTrace();
		}

		return flag;
	}

	public List<Map> loadAlldata(String databaseName, String tableName)
			throws Exception {

		List<Map> result = new ArrayList<Map>();
		Gson gson = new Gson();

		FileInputStream fis = new FileInputStream(databaseName);
		SAXBuilder builder = new SAXBuilder();
		Document document = builder.build(fis);
		Element root = document.getRootElement();

		List<Element> list = root.getChildren();

		for (Element e : list) {
			if (e.getAttributeValue("name").equals(tableName)) {
				List<Element> reclist = e.getChildren();
				for (Element rec : reclist) {
					Map map = gson.fromJson(rec.getText(), Map.class);
					result.add(map);
				}
			}
		}
		return result;
	}

	public boolean addData(String databaseName, String tableName, Object obj)
			throws Exception {
		boolean flag = false;
		Gson gson = new Gson();
		String json = gson.toJson(obj);
		FileInputStream fis = new FileInputStream(databaseName);
		SAXBuilder builder = new SAXBuilder();
		Document document = builder.build(fis);
		Element root = document.getRootElement();
		List<Element> list = root.getChildren();
		for (Element e : list) {
			if (e.getAttributeValue("name").equals(tableName)) {
				flag = true;
				Map newmap = new LinkedHashMap();
				newmap.put(_uuid, UUID.randomUUID().toString());
				Map objmap = gson.fromJson(json, LinkedHashMap.class);
				newmap.putAll(objmap);
				Element data = new Element("data");
				data.setText(gson.toJson(newmap, LinkedHashMap.class));
				e.addContent(data);
				break;
			}
		}

		FileOutputStream fos = new FileOutputStream(databaseName);
		XMLOutputter out = new XMLOutputter();
		out.output(document, fos);
		return flag;
	}

	public boolean updateData(String databaseName, String tableName, Object obj)
			throws Exception {
		boolean flag = false;
		Gson gson = new Gson();
		String json = gson.toJson(obj);
		Map newMap = gson.fromJson(json, Map.class);

		FileInputStream fis = new FileInputStream(databaseName);
		SAXBuilder builder = new SAXBuilder();
		Document document = builder.build(fis);
		Element root = document.getRootElement();
		List<Element> list = root.getChildren();

		for (Element e : list) {
			if (e.getAttributeValue("name").equals(tableName)) {
				List<Element> reclist = e.getChildren();
				for (Element rec : reclist) {
					Map currMap = gson.fromJson(rec.getText(), Map.class);
					if (newMap.get(_uuid).equals(currMap.get(_uuid))) {
						flag = true;
						// currMap.putAll(newMap);
						Element data = new Element("data");
						data.addContent(json);
						e.removeContent(rec);
						e.addContent(data);
						break;
					}
				}
				break;
			}
		}

		FileOutputStream fos = new FileOutputStream(databaseName);
		XMLOutputter out = new XMLOutputter();
		out.output(document, fos);
		return flag;
	}

	public boolean deleteData(String databaseName, String tableName, Object obj)
			throws Exception {
		boolean flag = false;
		Gson gson = new Gson();
		String json = gson.toJson(obj);
		Map deleting = gson.fromJson(json, Map.class);

		FileInputStream fis = new FileInputStream(databaseName);
		SAXBuilder builder = new SAXBuilder();
		Document document = builder.build(fis);
		Element root = document.getRootElement();
		List<Element> list = root.getChildren();

		for (Element e : list) {
			if (e.getAttributeValue("name").equals(tableName)) {
				List<Element> reclist = e.getChildren();
				for (Element rec : reclist) {
					Map current = gson.fromJson(rec.getText(), Map.class);
					if (deleting.get(_uuid).equals(current.get(_uuid))) {
						flag = true;
						e.removeContent(rec);
						break;
					}
				}
				break;
			}
		}

		FileOutputStream fos = new FileOutputStream(databaseName);
		XMLOutputter out = new XMLOutputter();
		out.output(document, fos);
		return flag;
	}
}
